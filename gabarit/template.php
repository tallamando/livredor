<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $titre ?></title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <div id="conteneur">
            <header>
            <h1>Livre d'or</h1>
                
            </header>

            <section>

            <?php echo $contenu ; ?>

            </section>

            <footer>
                <p>Copyright T. Allamando - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>