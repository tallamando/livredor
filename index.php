<?php 
$titre = "livre d'or";
ob_start();
session_start();
require "bdd/bddconfig.php";
try {
        $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname; charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $listeMessages = $objBdd->query("SELECT * FROM livreor");
           } catch (Exception $prmE) { die('Erreur : ' . $prmE->getMessage()); }
?>
    <link href="css/styles.css" rel="stylesheet" type="text/css"/>


    <article>                
        <h1>Messages du livre d'or</h1>
        <?php
    while ($msg = $listeMessages->fetch()) {
    ?>

        <h3><?php echo $msg['auteur']; ?></h3>
        <p><?= $msg['message']; ?></p>
        <p><sub><?= '(' . $msg['dateMessage'] . ')'; ?></sub></p>
        

    <?php
    }//fin du while
    $listeMessages->closeCursor(); //libère les ressources de la BDD
    ?>




        <form method="POST" action="index-signature.php">
                Nom :<input type="text" name="nom" required size="50">
                Message :<textarea name="message" id="message" required cols="50" rows="10"></textarea>
                <input type="submit" value="Valider">
        </form>
    </article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>